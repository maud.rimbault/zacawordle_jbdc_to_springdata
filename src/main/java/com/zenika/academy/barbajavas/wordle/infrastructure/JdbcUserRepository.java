package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.users.EmailAlreadyUsedException;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class JdbcUserRepository implements UserRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(User u) {
        try {
            jdbcTemplate.update("insert into users (tid, email, username) VALUES (?, ?, ?) on conflict(tid) do update set username=?",
                    u.getTid(), u.getEmail(), u.getUsername(), u.getUsername());
        } catch (DuplicateKeyException e) {
            // On sait que la seule contrainte d'unicité est sur l'email car on a géré les conflits de primary key dans la requête
            throw new EmailAlreadyUsedException();
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject("select * from users where email=?", new UserRowMapper(), email));
        } catch (IncorrectResultSizeDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByTid(String userTid) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject("select * from users where tid=?", new UserRowMapper(), userTid));
        } catch (IncorrectResultSizeDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(String userTid) {
        jdbcTemplate.update("delete from users where tid=?", userTid);
    }
}
