package com.zenika.academy.barbajavas.wordle.domain.model.game;

public enum ValidationLetter {
    GOOD_POSITION,
    WRONG_POSITION,
    NOT_IN_WORD
}
