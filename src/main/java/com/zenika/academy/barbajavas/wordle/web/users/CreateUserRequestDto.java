package com.zenika.academy.barbajavas.wordle.web.users;

public record CreateUserRequestDto(String email, String username) {
}
