package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;

import java.util.Optional;

public interface UserRepository {
    void save(User u);
    Optional<User> findByEmail(String email);
    Optional<User> findByTid(String userTid);
    void delete(String userTid);
}
