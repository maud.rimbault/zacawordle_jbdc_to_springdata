package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class JdbcGameRepository implements GameRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcGameRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(Game game) {
        jdbcTemplate.update("insert into games (tid, word, max_attempts, user_tid) VALUES (?, ?, ?, ?) on conflict do nothing",
                game.getTid(),
                game.getWord(true),
                game.getMaxAttempts(),
                game.getUserTid().orElse(null)
        );

        for (int i = 0; i < game.getRounds().size(); i++) {
            jdbcTemplate.update(
                    "insert into round_results(game_tid, round_order, letters) VALUES (?, ?, ?) on conflict do nothing",
                    game.getTid(),
                    i + 1,
                    String.valueOf(game.getRounds().get(i).letters())

            );
        }
    }

    @Override
    public Optional<Game> findByTid(String tid) {
        return jdbcTemplate.query(
                "select * from games g left join round_results r on r.game_tid=g.tid where tid=? order by round_order asc",
                new GameResultSetExtractor(),
                tid
        );
    }

    @Override
    public List<Game> findByUserTid(String userTid) {
        return jdbcTemplate.query(
                "select * from games g left join round_results r on r.game_tid=g.tid where user_tid=? order by game_tid, round_order asc",
                new GameListResultSetExtractor(),
                userTid
        );
    }
}
