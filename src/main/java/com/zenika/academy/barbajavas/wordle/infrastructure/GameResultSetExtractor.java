package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.game.GameNotForThisUserException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class GameResultSetExtractor implements ResultSetExtractor<Optional<Game>> {

    @Override
    public Optional<Game> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Game game = null;
        
        while(rs.next()) {
               if(game == null) {
                   game = new Game(rs.getString("tid"), rs.getString("word"), rs.getInt("max_attempts"), rs.getString("user_tid"));
               }
               
               // If there is a round result
               if(StringUtils.hasText(rs.getString("letters"))) {
                   try {
                       game.guess(rs.getString("letters"));
                   } catch (GameNotForThisUserException e) {
                       throw new IllegalStateException("Bad data in db");
                   }
               }
        }
        
        return Optional.ofNullable(game);
    }
}
