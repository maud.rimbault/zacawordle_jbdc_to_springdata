package com.zenika.academy.barbajavas.wordle.domain.model.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Game {
    private String tid;
    private String word;
    private int maxAttempts;
    private List<String> userGuesses;
    private String userTid;

    public Game(String tid, String word, int maxAttempts, String userTid) {
        this.userTid = userTid;
        this.tid = tid;
        this.word = word;
        this.maxAttempts = maxAttempts;
        this.userGuesses = new ArrayList<>(maxAttempts);
    }

    public String getTid() {
        return this.tid;
    }

    public Optional<String> getUserTid() {
        return Optional.ofNullable(this.userTid);
    }

    public int getAttemptsLeft() {
        return this.maxAttempts - this.getRounds().size();
    }

    public GameState getGameState() {
        if (this.getRounds().isEmpty()) {
            return GameState.IN_PROGRESS;
        } else if (this.getRounds().get(this.getRounds().size() - 1).isWin()) {
            return GameState.WIN;
        } else {
            return this.getRounds().size() < maxAttempts ? GameState.IN_PROGRESS : GameState.LOSS;
        }
    }

    public void guess(String userInput) throws GameNotForThisUserException {
        this.guess(userInput, null);
    }

    public void guess(String userInput, String userTid) throws GameNotForThisUserException {
        if(Objects.equals(this.userTid, userTid)) {
            this.userGuesses.add(userInput);
        }
        else {
            throw new GameNotForThisUserException();
        }
    }

    public int getWordLength() {
        return word.length();
    }

    public List<RoundResult> getRounds() {
        return this.userGuesses.stream()
                .map(l -> RoundResult.fromGuess(this.word, l))
                .collect(Collectors.toList());
    }

    public String getWord() {
        return this.getGameState() == GameState.IN_PROGRESS ? "?" : this.word;
    }

    public String getWord(boolean force) {
        if (force) {
            return this.word;
        }
        else {
            return this.getWord();
        }
    }

    public Set<Character> getInvalidLetters() {
        return this.getRounds().stream()
                .flatMap(rr -> rr.invalidLetters().stream())
                .collect(Collectors.toSet());
    }

    public int getMaxAttempts() {
        return this.maxAttempts;
    }
}
