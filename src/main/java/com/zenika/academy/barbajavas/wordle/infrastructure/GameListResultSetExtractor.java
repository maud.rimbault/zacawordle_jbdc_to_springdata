package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.game.GameNotForThisUserException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GameListResultSetExtractor implements ResultSetExtractor<List<Game>> {

    @Override
    public List<Game> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Game> games = new ArrayList<>();
        
        Game currentGame = null;
        while(rs.next()) {
               if(currentGame == null || !currentGame.getTid().equals(rs.getString("tid"))) {
                   games.add(currentGame);
                   currentGame = new Game(rs.getString("tid"), rs.getString("word"), rs.getInt("max_attempts"), rs.getString("user_tid"));
               }
               
               // If there is a round result
               if(StringUtils.hasText(rs.getString("letters"))) {
                   try {
                       currentGame.guess(rs.getString("letters"));
                   } catch (GameNotForThisUserException e) {
                       throw new IllegalStateException("Bad data in db");
                   }
               }
        }
        games.add(currentGame);
        
        return games;
    }
}
