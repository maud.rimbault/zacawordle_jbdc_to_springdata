create table if not exists users(
    tid char(36) primary key,
    email text not null unique,
    username text not null
);

create table if not exists games(
    tid char(36) primary key,
    word text not null,
    max_attempts int not null,
    user_tid char(36) references users(tid)
);

create table if not exists round_results(
    game_tid char(36) not null references games(tid),
    round_order int check (round_order > 0),
    letters text not null,
    primary key (game_tid, round_order)
);