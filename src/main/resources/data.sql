insert into users(tid, email, username) VALUES ('6a28ebdd-5b74-49f5-ab9d-f5a96e1f50f4', 'benoit.averty@zenika.com', 'Benoit') on conflict do nothing;

insert into games values ('20dfff6e-0547-4fb1-93e7-28a4a2667860', 'CRANE', 5, null) on conflict do nothing;

insert into round_results values('20dfff6e-0547-4fb1-93e7-28a4a2667860', 1, 'CRANS') on conflict do nothing;
insert into round_results values('20dfff6e-0547-4fb1-93e7-28a4a2667860', 2, 'CRANE') on conflict do nothing;